//
//  StudySetViewController.swift
//  CS422L
//
//  Created by Student Account  on 2/14/22.
//

import UIKit

class StudySetViewController: UIViewController {
    
    var cards: [Flashcard] = [Flashcard]()
    var i: Int = 0
    var isClicked: Bool = false
    var missed: Int = 0
    var correct: Int = 0
    var completed: Int = 0
    var max: Int = 0
    
    var missedFlashcards: [Flashcard] = [Flashcard]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cards = Flashcard.getHardCodedCollection()
        self.setupLabelTap()
        showFlashcard()
    }
    
    func setupLabelTap() {
        max = cards.count
        maxNum.text = String(max)
        maxNum.textAlignment = .center
        completedNum.textAlignment = .center
        term_definition.textAlignment = .center
        term_definition.isUserInteractionEnabled = true
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.labelTapped(_:)))
        self.term_definition.isUserInteractionEnabled = true
        self.term_definition.addGestureRecognizer(labelTap)
            
        }
    
    @objc func labelTapped(_ sender: UITapGestureRecognizer) {
            isClicked = !isClicked
            showFlashcard()
        }
    
    func showFlashcard() {
        numMissed.text = String(missed)
        numCorrect.text = String(correct)
        completedNum.text = String(completed)
        
        if(isClicked == false && !cards.isEmpty){
            term_definition.text = cards[i].term
        } else if(isClicked == true && !cards.isEmpty){
            term_definition.text = cards[i].definition
        }
        
    }
    @IBOutlet weak var term_definition: UILabel!
    @IBOutlet weak var numMissed: UILabel!
    @IBOutlet weak var numCorrect: UILabel!
    @IBOutlet weak var completedNum: UILabel!
    @IBOutlet weak var maxNum: UILabel!
    
    @IBAction func ExitButton(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }

    @IBAction func MissedButton(_ sender: Any) {
        if(!cards.isEmpty){
        let element = cards.remove(at: i)
        cards.insert(element, at: cards.count)
        missedFlashcards.append(element)
        
        missed += 1
        isClicked = false
        showFlashcard()
        }
    }
    
    @IBAction func SkipButton(_ sender: Any) {
        if(!cards.isEmpty){
        let element = cards.remove(at: i)
        cards.insert(element, at: cards.count)
        
        isClicked = false
        showFlashcard()
        }
    }
    
    @IBAction func CorrectButton(_ sender: Any) {
        if(!cards.isEmpty){
            var beenMiss: Bool = false
            for element in missedFlashcards {
                if(cards[i] === element){
                    beenMiss = true
                }
            }
            if(beenMiss == false){
                correct += 1
            }
            
            completed += 1
            isClicked = false
            cards.remove(at: i)
            showFlashcard()
        } else{
            isClicked = false
            completed = max
            term_definition.text = "set completed"
            showFlashcard()
        }
    }
}
