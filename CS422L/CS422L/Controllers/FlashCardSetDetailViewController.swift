//
//  FlashCardSetDetailActivity.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/3/21.
//

import Foundation
import UIKit

class FlashCardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    var cards: [Flashcard] = [Flashcard]()
    @IBOutlet var buttonView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var studyButton: UIButton!
    @IBOutlet var addButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cards = Flashcard.getHardCodedCollection()
        tableView.delegate = self
        tableView.dataSource = self
        makeItPretty()
    }
    
    //adds card
    @IBAction func addCard(_ sender: Any) {
        let newCard = Flashcard()
        newCard.term = "Term \(cards.count + 1)"
        newCard.definition = "Definition \(cards.count + 1)"
        cards.append(newCard)
        tableView.reloadData()
        tableView.scrollToRow(at: IndexPath(item: tableView.numberOfRows(inSection: tableView.numberOfSections - 1) - 1, section: tableView.numberOfSections - 1), at: .bottom, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath) as! FlashcardTableViewCell
        cell.flashcardLabel.text = cards[indexPath.row].term
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alert = UIAlertController(title: cards[indexPath.row].term, message: cards[indexPath.row].definition, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Edit", style: .default, handler: { [self] _ in
            let editAlert = UIAlertController(title: self.cards[indexPath.row].term, message: cards[indexPath.row].definition, preferredStyle: .alert)
            
            editAlert.addTextField(configurationHandler: {(textField) in textField.placeholder = cards[indexPath.row].term}
            )
            
            editAlert.addTextField(configurationHandler: {(textField) in textField.placeholder = cards[indexPath.row].definition}
            )
            
            let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { _ in
                cards.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            editAlert.addAction(deleteAction)
            
            let doneAction = UIAlertAction(title: "Done",style: .default) { [weak editAlert] _ in
                guard let textFields = editAlert?.textFields else { return }
                                                
                if let termText = textFields[0].text,
                   let definitionText = textFields[1].text {
                    cards[indexPath.row].term = termText
                    cards[indexPath.row].definition = definitionText
                    tableView.reloadData()
                    }
            }

            editAlert.addAction(doneAction)
            
            present(editAlert, animated: true)
        }))
        present(alert, animated: true)
    }
    
    //just a function to make everything look nice
    func makeItPretty()
    {
        buttonView.layer.cornerRadius = 8.0
        buttonView.layer.borderColor = UIColor.purple.cgColor
        buttonView.layer.borderWidth = 2.0
        deleteButton.layer.cornerRadius = 8.0
        studyButton.layer.cornerRadius = 8.0
        addButton.layer.cornerRadius = 8.0
    }
}
